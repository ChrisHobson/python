"""Draw the Dunns chamber using Turtle.
"""
import turtle as t
import sys
from dunns.dunnsdata import get_dunns
from maths.matrix import Matrix4x4

angles = [0, 0, 0]
matrix = None


def create_matrix():
    global matrix
    matrix = Matrix4x4.create_x_rotation(angles[0]).concat(
        Matrix4x4.create_y_rotation(angles[1])
    ).concat(
        Matrix4x4.create_z_rotation(angles[2])
    )


def xform_point(p):
    p2 = [p[0], p[1], p[2]]
    matrix.xform_point(p2)
    return p2


def draw_line(p1, p2):

    p = xform_point(p1)
    t.teleport(p[0] * 4, p[1] * 4)

    p = xform_point(p2)
    t.goto(p[0] * 4, p[1] * 4)

def x_key():
    print("X pressed")
    global angles
    angles[0] += .1
    create_matrix()
    redraw()


def y_key():
    print("Y pressed")
    global angles
    angles[1] += .1
    create_matrix()
    redraw()


def z_key():
    print("Z pressed")
    global angles
    angles[2] += .1
    create_matrix()
    redraw()


def q_key():
    print("Q pressed")
    sys.exit(0)


def redraw():
    t.clear()
    t.color('teal')
    t.width(1)
    t.setundobuffer(None)
    t.hideturtle()
    t.tracer(150000)
    for line in get_dunns():
        draw_line(line[0], line[1])

    t.color('red')
    t.width(3)
    draw_line((0, 0, 0), (50, 0, 0))

    t.color('green')
    draw_line((0, 0, 0), (0, 50, 0))

    t.color('blue')
    draw_line((0, 0, 0), (0, 0, 50))

    t.update()
    t.listen()


def dunns_turtle():
    create_matrix()
    redraw()
    t.onkey(x_key, "x")
    t.onkey(y_key, "y")
    t.onkey(z_key, "z")
    t.onkey(q_key, "q")
    t.mainloop()