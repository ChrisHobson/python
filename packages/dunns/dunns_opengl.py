"""Draw the Dunns combustion chamber using OpenGL.
"""
from OpenGL import *
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

from dunns.dunnsdata import get_dunns
from maths.azel import AZEL


class DunnsOpenGL:
    """Draw the Dunns chamber using OpenGL. Initially figure out how to use
    glut and OpenGL.
    """
    def __init__(self):
        self._show_screen_count = 0
        self._dunns = get_dunns()
        self._azel = AZEL()

    def dunns_opengl(self, x_args):
        """The main event, draw the combustion chamber.
        """
        glutInit()
        glutInitDisplayMode(GLUT_RGBA)
        glutInitWindowSize(500, 500)
        glutInitWindowPosition(0, 0)
        wind = glutCreateWindow("OpenGL Dunns")

        glutDisplayFunc(self.show_screen)
        glutKeyboardFunc(self.keyboard)
        glutSpecialFunc(self.special_keyboard)
        glutMouseFunc(self.mouse)

        glutMainLoop()

    def show_screen(self):
        """The function called by glut to show (redraw) the screen.
        """
        self._show_screen_count += 1
        width = glutGet(GLUT_WINDOW_WIDTH)
        height = glutGet(GLUT_WINDOW_HEIGHT)
        print(f"show_screen called {self._show_screen_count} {width}x{height}")
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        for corner in [0, 200]:
            glViewport(corner, corner, 200, 200)
            # - What are the matrix modes for
            # - How do we keep the chamber square
            # when we drag the window to be a rectangle
            # the glViewport does that but what sizes do we need
            # comment out glViewport to see what happens, or make
            # the glViewport coordinates not be a square.
            # glViewPort allows you to chop the graphics window
            # in to separate views... I think, draw twice to
            # show that happening.
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()
            glOrtho(-60, 60, -60, 60, -500, 300)
            glMatrixMode(GL_MODELVIEW)
            glLoadIdentity()
            xyz = [0, 0, 0]
            self._azel.get_pos(100, xyz)
            gluLookAt(xyz[0], xyz[1], xyz[2], 0, 0, 0, 0, 0, 1)
            glBegin(GL_LINES)
            for line in self._dunns:
                glVertex3f(line[0][0], line[0][1], line[0][2])
                glVertex3f(line[1][0], line[1][1], line[0][2])
            glEnd()
        glutSwapBuffers()

    def keyboard(self, key, x, y):
        """Key has been pressed.

        q - quit (or at least leave the main loop)
        esc - as per 'q'
        """
        # key is a bytes object so take the first character
        # as only (currently) dealing with normal keys
        ascii_char = chr(key[0])
        print(f"Keyboard. Key {ascii_char} X {x} Y {y}")
        if ascii_char == 'q' or ord(ascii_char) == 27:
            # 27 is the escape key
            print("Leaving Main Loop")
            glutLeaveMainLoop()

    def special_keyboard(self, key, x, y):
        """A special key has been pressed.

        Use the value GLUT_KEY_UP etc. to determine what to do
        """
        print(f"Special Keyboard. Key {key} X {x} Y {y}")
        if key == GLUT_KEY_UP:
            self._azel.el = self._azel.el + 5
        elif key == GLUT_KEY_DOWN:
            self._azel.el = self._azel.el - 5
        elif key == GLUT_KEY_RIGHT:
            self._azel.az = self._azel.az + 5
        elif key == GLUT_KEY_LEFT:
            self._azel.az = self._azel.az - 5

        glutPostRedisplay()


    def mouse(self, button, state, x, y):
        """Glut mouse function."""
        print(f"Mouse. Button {button} State {state} X {x} Y {y}")
