"""My Original Java CrossitGame converted to Python.

See https://bitbucket.org/ChrisHobson/java/src/master/crossit/src/com/chrishobson/crossit/CrossGame.java

Extra levels that I never solved in the original hand held game were taken from

https://www.jaapsch.net/puzzles/lights.htm
"""
from abc import ABC, abstractmethod


class CrossControl(ABC):
    def __init__(self, game, xy: (int, int)):
        """Setup the gaming engine and this controls xy.

        Call this from derived classes actual implementations
        """
        self.game = game
        self.xy = xy

    @abstractmethod
    def state(self) -> bool:
        """Returns the state of the control, True if on()
        """
        pass

    def toggle(self) -> None:
        """Swap the state of the control.

        If state() returns True, call off() else call on()
        """
        if self.state():
            self.off()
        else:
            self.on()

    @abstractmethod
    def off(self) -> None:
        """Turn off the control.
        """
        pass

    @abstractmethod
    def on(self) -> None:
        """Turn on the control.
        """
        pass

    def hit(self):
        """Control has been hit, tell gaming engine

        Normally the control invokes this method on itself, if must be
        prepared for state(), on() and off() being called whilst
        hit() is being processed.
        """
        self.game.handle_hit(self)


class CrossMessage(ABC):
    """Abstract class for messages from CrossGame.
    """

    @abstractmethod
    def message(self, msg):
        """Display the message string.
        """
        pass

    @abstractmethod
    def win(self):
        """Game has been won, application does whatever it wants.

        PLay a tune, do a dance.... it's your game your way!
        """
        pass


class CrossGame:
    """The class which controls the game play.

    It requires that CrossControl items have been made and given to the
    CrossGame so that there is a level of abstract between the gaming engine
    and the physical game display

    The last game in the m_games array is a special game not in the LightsOut
    original. It lights all 25 buttons, I've seen other Java games which
    play like this so I've added it as a special variant.
    """
    # The game size in X
    NUM_X = 5
    # The game size in Y
    NUM_Y = 5
    # Offsets used to access the 5 buttons to change
    # this, left, above, below, right
    OFFSETS = ((0, 0), (-1, 0), (1, 1), (0, -2), (1, 1))

    GAMES = (
        (11, 13, 15),
        (21, 16, 6, 1, 23, 18, 8, 3, 25, 20, 10, 5),
        (16, 11, 6, 22, 17, 12, 7, 2, 24, 19, 14, 9, 4, 20, 15, 10),
        (21, 16, 6, 22, 7, 24, 9, 25, 20, 10),
        (21, 11, 6, 1, 22, 12, 7, 2, 13, 8, 3, 24, 19, 4, 25, 20, 15, 10),
        #
        (16, 11, 22, 23, 18, 13, 24, 20, 15),
        (21, 16, 11, 6, 1, 22, 2, 23, 3, 24, 4, 20, 15, 10),
        (16, 22, 12, 18, 8, 24, 14, 20),
        (21, 6, 22, 17, 12, 7, 2, 23, 13, 8, 19, 14, 9, 4, 20, 10),
        (12, 7, 2, 13, 8, 3, 14, 9, 4),
        #
        (16, 11, 6, 1, 22, 23, 18, 13, 8, 3, 24, 20, 15, 10, 5),
        (11, 1, 22, 17, 12, 7, 2, 18, 3, 24, 19, 14, 9, 4, 15, 5),
        (16, 22, 12, 18, 8, 14, 4, 10),
        (22, 17, 12),
        (17, 7),
        #
        (21, 16, 11, 6, 1, 22, 23, 24, 25),
        (21, 22, 17, 23, 18, 13, 24, 19, 25),
        (11, 17, 7, 23, 13, 3, 19, 9, 15),
        (21, 11, 1, 23, 13, 3, 25, 15, 5),
        (11, 15),
        #
        (22, 17, 12, 7, 2, 13, 3, 14, 4, 5),
        (16, 11, 6, 22, 2, 23, 3, 24, 4, 20, 15, 10),
        (23, 18, 13, 19, 14, 15),
        (16, 11, 22, 17, 18, 19, 25, 20, 15),
        (1, 6, 7, 11, 12, 13, 16, 17, 18, 19, 22, 23, 24, 25),
        #
        (1, 5, 6, 10, 11, 12, 13, 14, 15, 16, 20, 21, 25),
        (3, 7, 8, 9, 13, 18, 23),
        (13, 14, 15, 18, 19, 20, 23, 24, 25),
        (7,),
        (13,),
        #
        (1, 5, 6, 7, 10, 11, 13, 15, 16, 19, 20, 21, 25),
        (1, 2, 3, 4, 5, 9, 13, 17, 21, 22, 23, 24, 25),
        (4, 9, 11, 13, 15, 16, 20, 21, 24, 25),
        (3, 5, 6, 10, 11, 15, 17, 18, 20, 22, 23, 24, 25),
        (4, 5, 7, 9, 11, 16, 18, 20),
        #
        (3, 7, 9, 11, 15, 16, 17, 18, 19, 20, 21, 25),
        (7, 8, 9, 12, 13, 14, 17, 18, 19),
        (1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25),
        (2, 4, 6, 11, 12, 18, 19, 22, 24),
        (12, 14),
        #
        (1, 5, 7, 9, 13, 18, 23),
        (1, 2, 3, 6, 9, 11, 12, 13, 16, 19, 21, 22, 23),
        (1, 5, 6, 7, 9, 11, 12, 13, 17, 22, 23, 24),
        (6, 7, 9, 10, 11, 12, 13, 14, 15, 18, 22, 23, 24),
        (2, 3, 4, 6, 8, 13, 14, 15, 16, 17, 18, 19, 21, 23, 25),
        #
        (3, 7, 8, 9, 11, 12, 13, 14, 15, 17, 18, 19, 23),
        (3, 6, 7, 8, 9, 10, 11, 13, 17, 20, 25),
        (6, 10, 13, 16, 20),
        (1, 5, 7, 9, 13, 17, 19, 21, 25),
        (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
         21, 22, 23, 24, 25),
    )

    def __init__(self):
        # Which game are we playing
        self.game_id = 1
        self.controls = None
        self.message = None
        self.num_moves = 0

    def set_controls(self,
                     message_control: CrossMessage,
                     controls):
        """Set the concrete version of the message control and buttons

        Arguments
        message_control A derived CrossMessage suitable for application

        controls An array of arrays, the numbering of controls is as per
        this diagram.

        NOTE: This is wrong, it should be X that is the 1st array
        with element of the X array being a Y array.




        [0][0] [0][1] [0][2] [0][3] [0][4]
        [1][0] [1][1] [1][2] ...
        [2][0]
        [3][0]
        [4][0]                      [4][4]

        So the 1st index being the row, and the second index the column
        with [0] being the top row and [4] being the last row.

        Controls can also be consider to be numbered from 1 to 25 as per

        1   2   3   4   5
        6              10
        11             15
        16             20
        21 22          25

        This numbering is more of an internal convention used to configure
        the active controls for each level.
        """
        self.controls = controls
        self.message = message_control

    def handle_hit(self, control: CrossControl):
        x, y = control.xy
        for xy in self.OFFSETS:
            x += xy[0]
            y += xy[1]
            if 0 <= x < self.NUM_X and 0 <= y < self.NUM_Y:
                self.controls[x][y].toggle()
        self.num_moves += 1
        self.status()

    def set_game(self, game_id):
        """Set which game to play, and start it.

        Game IDs are 1 based, even though the internal array is 0 based.
        """
        self.game_id = game_id
        self.start_game()

    def start_game(self):
        """Start the current game
        """
        self.clear_board()
        self.num_moves = 0
        for id in self.GAMES[self.game_id - 1]:
            x, y = CrossGame.control_id_to_xy(id, self.NUM_X, self.NUM_Y)
            self.controls[x][y].on()

        self.status()

    def clear_board(self):
        """Sets all controls to off()
        """
        for a in self.controls:
            for control in a:
                control.off()

    def num_on(self):
        """How many controls are on().
        """
        num = 0
        for a in self.controls:
            for control in a:
                if control.state():
                    num += 1

        return num

    def status(self):
        """A placeholder game status, reports to the message object.
        """
        min = (self.game_id + 4)//5 + 5
        self.message.message(f"Level {self.game_id} - {self.num_moves} Moves (Min = {min})")
        if self.num_moves == min and self.num_on() == 0:
            self.message.win()

    @staticmethod
    def control_id_to_xy(id: int, num_x: int, num_y: int) -> (int, int):
        """Convert a square id into its xy based on the grid size.

        The square id is the 1 based number of the square starting top left
        of the game board and moving to the right. An example of a 3x2 grid:

        1 2 3   (0,0) (1,0) (2,0)
        4 5 6   (0,1) (1,1) (2,1)

        :param id  The game control id
        :param num_x  The grid size in x
        :param num_y  The grid size in y

        :returns A tuple of the controls X, Y
        """
        assert 0 < id <= num_x * num_y, "id isn't valid for grid size"

        y = (id - 1) // num_x
        x = id - 1 - num_x * y
        return x, y
