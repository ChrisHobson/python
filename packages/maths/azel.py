"""Matrix classes.
"""
from math import cos, sin, degrees, radians, sqrt, isclose


class AZEL():
    """An azimuth and elevation.

    Stores (in degrees) AZ and EL and provides methods to manipulate them.
    Imagine a sphere the AZ (0-360) is the angle around the equator of the sphere
    and the EL in the angle above or below the equator (90 TO -90).
    """
    def __init__(self, az = 0.0, el = 0.0):
        """Initialise as 0, 0 by default or supply values.
        """
        self._az = az
        self._el = el

    def __str__(self):
        """Return a printable form.
        """
        return f"AZ = {self._az} EL = {self._el}"

    @property
    def az(self):
        return self._az

    @az.setter
    def az(self, az):
        self._az = az

    @property
    def el(self):
        return self._el

    @el.setter
    def el(self, el):
        self._el = el


    def get_pos(self, radius, xyz):
        """Given the radius of a sphere get the xyz that the angles are at.
        """
        # What would the radius of a unit sphere be
        # at the el angle
        #
        #    /
        # 1 /
        #  / el
        #  ---
        #   r
        rad = cos(radians(self._el))

        # Ensure x and Y are on the circle at that radius
        # an elevation of 90 will lead to x,y of 0,0
        x = cos(radians(self._az)) * rad
        y = sin(radians(self._az)) * rad
        # Z is the height of the circle
        z = sin(radians(self._el))

        # Check the above is normalised
        assert isclose(sqrt(x*x + y*y + z*z), 1), "Magnitude should be 1"

        # Scale to users chosen sphere
        xyz[0], xyz[1], xyz[2] = x * radius, y * radius, z * radius
