"""Matrix classes.
"""
from math import cos, sin

class Matrix4x4():
    """A 4x4 transformation matrix.

    This allows points and vectors to be transformed. Matrices can be
    concatenated such that one matrix can multiple transforms all at once.
    """
    def __init__(self):
        """Initialise as an identity matrix.
        """
        self._rows = list()
        self._rows.append([1, 0, 0, 0])
        self._rows.append([0, 1, 0, 0])
        self._rows.append([0, 0, 1, 0])
        self._rows.append([0, 0, 0, 1])

    def __str__(self):
        """Return a printable form.
        """
        return f"{self._rows[0]}\n{self._rows[1]}\n{self._rows[2]}\n{self._rows[3]}"

    def concat(self, second_xform):
        """Concat the two matrices.

        c = a.concat(b)

        creates a new matrix 'c' whose xform is as if a point
        had first been xformed by 'a' and then by 'b'.
        """
        # Reverse the ordering of the matrices as doing A x B will result
        # in B being applied first and then A. We want B x A to match what
        # the specification of the methods says.
        b = self
        a = second_xform
        c = Matrix4x4()
        for a_row in range(0, 4):
            for b_col in range(0, 4):
                val = a._rows[a_row][0] * b._rows[0][b_col] + \
                      a._rows[a_row][1] * b._rows[1][b_col] + \
                      a._rows[a_row][2] * b._rows[2][b_col] + \
                      a._rows[a_row][3] * b._rows[3][b_col]
                c._rows[a_row][b_col] = val

        return c

    def xform_point(self, xyz):
        """'in-place' transform the point by the matrix.

        'xyz' can be of any type so long as it supports
        getting and setting via [] and that xyz[0] is X and xyz[2] is Z.
        """
        vals = xyz[0], xyz[1], xyz[2], 1

        for i in range(0, 3):
            row = self._rows[i]
            xyz[i] = vals[0] * row[0] + vals[1] * row[1] + vals[2] * row[2] + vals[3] * row[3]


    @staticmethod
    def create_translation(xyz):
        matrix = Matrix4x4()
        for i in range(0, 3):
            matrix._rows[i][3] = xyz[i]

        return matrix

    @staticmethod
    def create_x_rotation(angle_radians):
        """Creates a matrix that rotates around X.

        Anti-Clockwise when viewing from high X towards the origin.
        """
        matrix = Matrix4x4()
        cos_a = cos(angle_radians)
        sin_a = sin(angle_radians)
        matrix._rows[1][1] = cos_a
        matrix._rows[1][2] = -sin_a
        matrix._rows[2][1] = sin_a
        matrix._rows[2][2] = cos_a

        return matrix

    @staticmethod
    def create_y_rotation(angle_radians):
        """Creates a matrix that rotates around Y.

        Anti-Clockwise when viewing from high Y towards the origin.
        """
        matrix = Matrix4x4()
        cos_a = cos(angle_radians)
        sin_a = sin(angle_radians)
        matrix._rows[0][0] = cos_a
        matrix._rows[0][2] = sin_a
        matrix._rows[2][0] = -sin_a
        matrix._rows[2][2] = cos_a

        return matrix

    @staticmethod
    def create_z_rotation(angle_radians):
        """Creates a matrix that rotates around Z.

        Anti-Clockwise when viewing from high Z towards the origin.
        """
        matrix = Matrix4x4()
        cos_a = cos(angle_radians)
        sin_a = sin(angle_radians)
        matrix._rows[0][0] = cos_a
        matrix._rows[0][1] = -sin_a
        matrix._rows[1][0] = sin_a
        matrix._rows[1][1] = cos_a

        return matrix

    @staticmethod
    def create_scale(factor):
        """Creates a matrix that scales (around the origin).
        """
        matrix = Matrix4x4()
        matrix._rows[0][0] = \
            matrix._rows[1][1] = \
            matrix._rows[2][2] = factor

        return matrix
