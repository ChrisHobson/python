"""Vector classes.
"""
from math import sqrt


class Vector3D():
    """A 3D vector.
    """
    VEC_I = 0
    VEC_J = 1
    VEC_K = 2

    def __init__(self, copy=None, axis=-1):
        """Initialise as either

        If 'copy' is given copy an existing vector using
        [0]...[2] to access the components.

        If 'axis' is given align a unit vector along i, j, or K. Use
        the VEC_I/J/K constants for clarity.
        """
        if copy:
            self._ijk = [copy[Vector3D.VEC_I], copy[Vector3D.VEC_J],copy[Vector3D.VEC_K]]
        else:
            self._ijk = [0, 0, 0]
            if axis > -1:
                assert axis >= 0 <= 2, "Invalid axis"
                self._ijk[axis] = 1

    def __getitem__(self, item):
        """Support [].
        """
        assert Vector3D.VEC_I <= item <= Vector3D.VEC_K, "Invalid component"
        return self._ijk[item]

    def __setitem__(self, key, value):
        """Support [].
        """
        assert Vector3D.VEC_I <= key <= Vector3D.VEC_K, "Invalid component"
        self._ijk[key] = value

    def __str__(self):
        """Return a printable form.
        """
        return f"I = {self._ijk[Vector3D.VEC_I]} J = {self._ijk[Vector3D.VEC_J]} K = {self._ijk[Vector3D.VEC_K]}"

    @property
    def i(self):
        return self._ijk[Vector3D.VEC_I]

    @i.setter
    def i(self, i):
        self._ijk[Vector3D.VEC_I] = i

    @property
    def j(self):
        return self._ijk[Vector3D.VEC_J]

    @j.setter
    def j(self, j):
        self._ijk[Vector3D.VEC_J] = j

    @property
    def k(self):
        return self._ijk[Vector3D.VEC_K]

    @k.setter
    def k(self, k):
        self._ijk[Vector3D.VEC_K] = k

    def cross(self, b, c=None):
        """Vector cross product.

        Create the cross product and assign to 'c' which will normally be
        a Vector3D but can be any object that supports []. This allows
        any object to be used that supports.

        If c is None a new Vector3D is created

        Returns 'c' which is either the original object or a new Vector3D
        """
        if not c:
            c = Vector3D()

        return Vector3D.cross_s(self, b, c)

        return out

    @staticmethod
    def cross_s(a, b, c):
        """Static Vector cross product.

        c = a x b

        a, b, c are 3 objects that support [].

        Returns c to allow chaining.
        """
        c[Vector3D.VEC_I] = a[Vector3D.VEC_J] * b[Vector3D.VEC_K] \
                              - a[Vector3D.VEC_K] * b[Vector3D.VEC_J]
        c[Vector3D.VEC_J] = a[Vector3D.VEC_K] * b[Vector3D.VEC_I] \
                              - a[Vector3D.VEC_I] * b[Vector3D.VEC_K]
        c[Vector3D.VEC_K] = a[Vector3D.VEC_I] * b[Vector3D.VEC_J] \
                              - a[Vector3D.VEC_J] * b[Vector3D.VEC_I]

        return c

    def dot(self, b):
        """Compute the dot product.
        """
        return Vector3D.dot_s(self, b)

    @staticmethod
    def dot_s(a, b):
        """Compute the dot product.
        """
        return a[Vector3D.VEC_I] * b[Vector3D.VEC_I] \
            + a[Vector3D.VEC_J] * b[Vector3D.VEC_J] \
            + a[Vector3D.VEC_K] * b[Vector3D.VEC_K]

    def normalise(self):
        """Make the vector have a length of 1.0
        """
        Vector3D.normalise_s(self._ijk)

    @staticmethod
    def normalise_s(vec):
        """A static version of normalise.
        """
        length = Vector3D.length_s(vec)
        if length > 0:
            vec[0] /= length
            vec[1] /= length
            vec[2] /= length

    def length(self):
        return Vector3D.length_s(self._ijk)

    @staticmethod
    def length_s(vec):
        """A static version of length.
        """
        return sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2])

    def project_to_plane(self, plane_normal):
        """
        Project the vector so that it lies on the plane.
        """
        Vector3D.project_to_plane_s(self, plane_normal)

    @staticmethod
    def project_to_plane_s(vec, plane_normal):
        """Project vec onto the plane.
        """
        # Magnitude of vector normal to plane, squared
        # dot_s will compute this to save doing it long hand in here
        vec_mag2 = Vector3D.dot_s(plane_normal, plane_normal);
        # Component of plane_normal to be subtracted from vec
        fac = Vector3D.dot_s(vec, plane_normal) / vec_mag2
        # Set required direction
        for i in range(0, 3):
            vec[i] = vec[i] - fac * plane_normal[i]
