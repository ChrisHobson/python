"""Over load ArgumentParser to add the execution code.

If you use 'ArgParserExec' then when parse_and_execute() is called
the arguments are parsed and a function called x_'cmd' is
called passing the parsed_args.

For a full automated scheme favour ArgParserExecProg which
autoloads a class based on the name of the program and the
name of the sub-command. The class instance is made and
its run(x_args) method is called.

These classes are used when add_subparsers() is used, which is the
best way to write commands of any size. add_subparsers()
provide 'git' style commands where a main command uses
sub-commands.
"""

from sys import exit
from sys import modules
from sys import argv
from pathlib import Path

from argparse import ArgumentParser
from golftools.utils.classes import make_class


class ArgParserExec(ArgumentParser):
    def parse_and_execute(self):
        """Parse the arguments and call the x_'cmd' global function.
        """
        parsed_args = self.parse_args()
        if hasattr(self, "_func"):
            func = self._func
        else:
            func_name = "x_" + parsed_args.cmd
            func = getattr(modules["__main__"], func_name, None)
            # Look up a function of the name x_cmd (x for execute)
            # func = globals().get("x_" + parsed_args.cmd, None)
        if not func:
            print(f"Error: Unknown command {parsed_args.cmd} Implement {func_name}")
            print(parsed_args)
        else:
            # Run the function passing the arguments
            try:
                func(parsed_args)
                exit(0)
            except Exception as error:
                print(f"Error: running '{parsed_args.cmd}'\n       {error}")

        exit(1)

    def set_handler(self, func):
        """Rather than calling x_'cmd' call func for all cmds.
        """
        self._func = func

class ArgParserExecAuto(ArgParserExec):
    """An ArgParser that autoloads classes.

    Takes the name of the program and the lowercase('cmd') to form a module name
    in which there should be a class called 'cmd'. Creates an instance
    of that class and calls its run() method passing the parsed arguments.

    The result is that you only need to write the command parsing and the class
    is dynamically loaded and run.

    See 'opengl' for an example.
    """
    def parse_and_execute(self):
        """Invokes the parser, creates a class instance and runs it.
        """
        self.set_handler(ArgParserExecAuto._handler)
        super().parse_and_execute()

    @staticmethod
    def _handler(x_args):
        def execute(x_args):
            """Auto create a class instance and invoked run(x_args).

            Load a class called 'x_args.ClassName'from a module called
            progname.lowercasedcmd

            and invokes the run(x_args) method of the class. The
            run method is potentially a @staticmethod but doesn't
            have to be.

            You set the class name to be created via

            parser.set_defaults(ClassName="Test")

            See progs/opengl and opengl/test.py for an example, which is run via

            opengl test

            This creates a class opengl.test.Test and calls run(x_args).
        """
        prog_name = Path(argv[0]).name.lower()
        module_name = f"{prog_name}.{x_args.cmd.lower()}"
        class_name = getattr(x_args, 'ClassName')
        sample = make_class(f"{module_name}.{class_name}")
        sample.run(x_args)
