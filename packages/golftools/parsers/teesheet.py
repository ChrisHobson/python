"""Classes to parse files which contains members on tee sheets.

"""

from golftools.utils.members import Members
from golftools.utils.members import Member
from golftools.parsers.members import MemberParser
from pathlib import Path


class StdTeeSheet(MemberParser):
    """A standard parser which reads a cut and paste tee sheet.

    On Windows cut and paste a start sheet in to a file and then this
    class will parse the names from it.
    """
    def __init__(self, file_name):
        """The constructor.
        """
        super().__init__()
        self._file_path = Path(file_name)

    def do_parse(self):
        """Derived method called from parse().

        Reads the tee sheet and extracts all Members.
        """
        with open(self._file_path) as f:
            for line in f:
                line = line.strip("\r\n")
                data = line.split("\t")
                # Skip the initial time field
                for player in data[1:]:
                    # Buttons to add members cut-and-paste as "Select"
                    if player != "Select":
                        self.add_member(Member(player))
