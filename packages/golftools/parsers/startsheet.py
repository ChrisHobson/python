"""Classes to parse files which contains members on start sheets.

"""

from golftools.utils.members import Members
from golftools.utils.members import Member
from golftools.parsers.members import MemberParser
from pathlib import Path


class StdStartSheet(MemberParser):
    """A standard parser which reads a cut and paste start sheet.
    """
    def __init__(self, file_name):
        """The constructor.
        """
        super().__init__()
        self._file_path = Path(file_name)

    def do_parse(self):
        """Derived method called from parse().

        Reads the start sheet and extracts all Members.
        """
        with open(self._file_path) as f:
            for line in f:
                line = line.strip("\r\n")
                data = line.split("\t")
                for player in data:
                    open_bracket = player.find(" (")
                    # Anything with a ( and a . is enough to locate
                    # a players name. We can refine this as we find
                    # special cases
                    if open_bracket > -1 and player.find(".") > -1:
                        name = player[:open_bracket]
                        self.add_member(Member(name))
