"""Classes to parse members from files which contain lists of members.

"""

from golftools.utils.members import Members
from golftools.utils.members import Member
from pathlib import Path


from golftools.utils.members import Members
from golftools.utils.members import Member
from pathlib import Path


class MemberParser:
    """A base class for all parsers of member names.

    Create the appropriate derived class and then call parse() to
    get the Members object.
    """
    def __init__(self):
        """Create a Members instance for all derived parsers to share.
        """
        self._members = Members()

    def members(self) ->Members:
        """Returns the members, normally use parse() though .
        """
        return self._members

    def parse(self) -> Members:
        """Base class method which calls derived class.
        """
        self.do_parse()
        return self.members()

    def add_member(self, member: Member):
        self._members.add_member(member)


class StdMembers(MemberParser):
    """A standard parse which reads a simple 1 member per line file.
    """
    def __init__(self, file_name):
        """The constructor.
        """
        MemberParser.__init__(self)
        self._file_path = Path(file_name)

    def do_parse(self):
        """Derived method called from parse().

        Reads the file and creates 1 Member per line.
        """
        with open(self._file_path) as f:
            for line in f:
                line = line.strip("\r\n")
                self.add_member(Member(line))


class CSV1Members(MemberParser):
    """A standard parse which reads a simple CSV member per line file.

    John Grant provides the file via an IG report. There is a header
    line which is ignored and then one member per line with the name
    surrounded by "'s
    """
    def __init__(self, file_name):
        """The constructor.
        """
        MemberParser.__init__(self)
        self._file_path = Path(file_name)

    def do_parse(self):
        """Derived method called from parse().

        Reads the file and creates 1 Member per line dumping
        the first line and removing quotes.
        """
        with open(self._file_path) as f:
            f.readline()
            for line in f:
                line = line.strip("\r\n").replace('"', "")
                self.add_member(Member(line))


class Entrants(MemberParser):
    """A reads a file on entered members.

    Members cut and paste from an IG entrants list. The contains lines of
    member name (hci)
    """
    def __init__(self, file_name):
        """The constructor.
        """
        MemberParser.__init__(self)
        self._file_path = Path(file_name)

    def do_parse(self):
        """Derived method called from parse().

        Reads the file get members names and handicaps.
        """
        with open(self._file_path) as f:
            for line in f:
                line = line.strip("\r\n")
                (name, hci) = line.split("(")
                hci = hci.strip(")")
                mem = Member(name)
                mem.hci = hci
                self.add_member(mem)

