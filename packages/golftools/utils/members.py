"""Members.

"""

class Member:
    """One member.

    Basically a thin wrapper to a name, class exists
    as later more data can be added. Note that Member
    objects are the same if their names are the same and
    if added as a key to a hash table two members of
    the same name are the same hash value.

    Any leading/trailing spaces are removed and multiple spaces
    are converted to a single space.
    """
    def __init__(self, name):
        self._name = " ".join(name.split())
        self._hci = None

    def __eq__(self, other):
        """Equals operator that matches on name.
        """
        if isinstance(other, Member):
            return self.name == other.name
        return NotImplemented

    def __hash__(self):
        """Allows addition to a hash table as the key.

        Member objects are considered the same if their names are the same.
        """
        return hash(self.name)

    @property
    def name(self):
        """Get the name.
        """
        return self._name

    @property
    def hci(self):
        """Gets the member's HCI
        """
        if self._hci is None:
            raise Exception("HCI never set")

        return self._hci

    @hci.setter
    def hci(self, hci):
        """Sets the member's HCI
        """
        self._hci= float(hci)



class Members:
    """Holds a list of member instances.
    """
    def __init__(self):
        """Creates the list to hold members.
        """
        self._members = list()

    def add_member(self, member: Member):
        """Adds a member.
        """
        self._members.append(member)

    def get_as_list(self):
        """Returns a standalone list of members.
        """
        return self._members.copy()

    def get_as_dict(self):
        """Returns a standalone dict/hash of members keyed by name.
        """
        return {mem.name: mem for mem in self._members}
