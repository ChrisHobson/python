"""golftools utilities for instantiating class instances etc.

"""

import importlib


def make_class(name, *args):
    """Dynamically load a module and instantiate a class instance.

    Given a . separate string 'my.module.name.ClassName' dynamically
    import 'my.module.name' and then instantiate 'ClassName'.

    Any arguments after 'name' are passed as individual arguments
    to the classes constructor.
    """
    # Split string at the .'s and take the last part as classname
    # and join the other parts back together for the module name.
    parts = name.split(".")
    module_name = ".".join(parts[:-1])
    class_name = parts[-1]
    # import the module containing class_name
    module = importlib.import_module(module_name)
    # Find the constructor
    class_constructor = getattr(module, class_name)
    # make the instance handing off the arguments
    return class_constructor(*args)
