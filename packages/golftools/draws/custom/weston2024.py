"""Custom draw code for the JAGS 2024 Weston Trip.

It's complicated!
"""

from pathlib import Path

from golftools.utils.members import Member
from golftools.draws.event import Draw


class Weston2024:
    def draw(self):
        print("Doing the draw....")
        players_path = Path("../../Dropbox/golf/jags/weston2024.txt")
        buggies_path = Path("../../Dropbox/golf/jags/weston_2024_buggy_shares.txt")
        for path in [players_path, buggies_path]:
            assert path.is_file(), f"Missing file {path}"

        # use a hash because the ID of the players
        # is in the file and isn't sequential, so we
        # index a hash by an integer
        players = {}
        # Read the players, their ID HCI and names
        with open(players_path) as f:
            while line := f.readline():
                data = line.split()
                player_id = int(data[1])
                player_hci = float(data[0])
                player = Member(" ".join(data[2:]))
                player.hci = player_hci
                assert player_id not in players, f"Duplicate ID {player_id}"
                players[player_id] = player
                # Monkey path our ID
                player.id = player_id
        num_players = len(players)
        # Pad with scratch golfers to make sure we have four balls
        while len(players) % 4 != 0:
            player_id = len(players) + 1
            dummy = Member(f"Dummy {next}")
            dummy.hci = 0
            dummy.id = player_id
            players[player_id] = dummy

        num_groups = len(players) // 4
        the_draw = Draw(3, num_groups)

        # Preload the draw with the buggy shares
        with open(buggies_path) as f:
            day = -1
            while line := f.readline():
                if "_" in line:
                    day += 1
                    group = -1
                elif "/" in line:
                    group += 1
                    data = line.split()
                    player_one = players[int(data[0])]
                    player_two = players[int(data[4])]
                    assert not the_draw.played_with(player_one, player_two), "Already played together"
                    the_draw[day][group].add_player(player_one)
                    the_draw[day][group].add_player(player_two)

        yewer = Member("Rob Yewer")
        yewer.hci = 19.1
        yewer.id = 27


        # Print the output
        day_num = 1
        for event in the_draw:
            print(f"Day {day_num}")
            day_num += 1
            group_num = 1
            for group in event:
                print(f"Group {group_num} ")
                group_num += 1
                for player in group:
                    print(f"{player.name}({player.id}) ",end="")
                print("")
