"""Classes for tier based draws.

"""

from random import shuffle
import sys

from golftools.draws.draw import Draw
from golftools.utils.members import Member


class Tiers(Draw):
    """A base class for all tier draw generators.

    """
    def __init__(self, entrants):
        self._entrants = entrants

    def entrants_list(self):
        return self._entrants.get_as_list()

    def do_draw(self):
        self._do_draw()

class RandomTiers(Tiers):
    """Splits in to tiers and randomises to minimise the HCI split.
    """
    def __init__(self, entrants, team_size, allow_padding):
        super().__init__(entrants)
        self._team_size = team_size
        self._allow_padding = allow_padding

    def _do_draw(self):
        entrants_list = self.entrants_list()
        team_size = self._team_size
        if not self._allow_padding and len(entrants_list) % team_size:
            raise Exception(f"Must be a multiple of {team_size} entrants")

        # Ensure we have a multiple of team_size players. Pad
        # with scratch golfers so that they go in tier one
        # to prevent 2/3 balls having a really low handicap
        # player who can take all the shots in a Florida
        # scramble.
        for mem_id in range(1, team_size):
            if len(entrants_list) % team_size == 0:
                break
            pad = Member(f"Another Player {mem_id}")
            pad.hci = 0.0
            entrants_list.append(pad)

        def sort_by_hci(mem):
            return mem.hci

        entrants_list.sort(key=sort_by_hci)

        per_tier = len(entrants_list) // team_size

        tiers = []
        start = 0
        end = per_tier
        for _ in range(0, team_size):
            tiers.append(entrants_list[start:end])
            start = end
            end += per_tier

        min_range = sys.float_info.max

        for x in range(0, 200000):
            for tier in tiers:
                shuffle(tier)

            min_hci = sys.float_info.max
            max_hci = sys.float_info.min
            for team in zip(*tiers, strict=True):
                hci_sum = 0
                for mem in team:
                    hci_sum += mem.hci
                min_hci = min(min_hci, hci_sum)
                max_hci = max(max_hci, hci_sum)

            draw_range = max_hci - min_hci
            if draw_range < min_range:
                print("")
                for team in zip(*tiers, strict=True):
                    for mem in team:
                        print(f"{mem.name} ({mem.hci:.1f}) ", end="")
                    print()
                print(f"Min HCIs = {min_hci:.1f} Max HCIs = {max_hci:.1f} Range = {draw_range:.1f}")
                min_range = draw_range
