"""Classes for dealing with a draw or schedule.

A draw is the entire games for the trip like the spain or jags
trip and is made up events which have multiple fourballs.

The classes help to determine if players have played together
already etc. They don't prevent people playing multiple times
together, they just allow you to determine if it has happened.
"""
from golftools.utils.members import Member, Members


class FourBall:
    """A holder for 4 players (Member instances).
    """
    def __init__(self):
        self._players = Members()

    def __contains__(self, player):
        """Allows 'if player in fourball' to be used.
        """
        return player in self._players.get_as_list()

    def __len__(self):
        """Allows 'len(fourball)' to be used.
        """
        return len(self._players.get_as_list())

    def __getitem__(self, item):
        """Allows 'fourball[2]' to be used.
        """
        return self._players.get_as_list()[item]

    def add_player(self, player: Member):
        """Adds the player to the FourBall.
        """
        assert len(self) < 4, "FourBall is already full"
        assert player not in self, f"{player.name} is already in the FourBall"

        self._players.add_member(player)

    def played_with(self, player_one, player_two):
        """Have the two players already played together in this group?
        """
        return player_one in self and player_two in self


class Event:
    """One event, typically a days play.

    An event has multiple fourballs, which represent the players
    in each tee time /group.
    """
    def __init__(self, num_fourballs):
        self._fourballs = [FourBall() for _ in range(0, num_fourballs)]

    def __len__(self):
        """Number of FourBalls.
        """
        return len(self._fourballs)

    def __getitem__(self, item):
        """Gets a zero based fourball via [].
        """
        return self._fourballs[item]

    def played_with(self, player_one, player_two):
        """Have the two players already played together in this event?
        """
        for group in self._fourballs:
            if group.played_with(player_one, player_two):
                return True
        return False


class Draw:
    """The entire draw for multiple events (or days).
    """
    def __init__(self, num_events, num_fourballs):
        self._events = [Event(num_fourballs) for _ in range(0, num_events)]

    def __len__(self):
        """Number of events (days).
        """
        return len(self._events)

    def __getitem__(self, item):
        """Gets a zero based fourball via [].
        """
        return self._events[item]

    def played_with(self, player_one, player_two):
        """Have the two players already played together?
        """
        for event in self._events:
            if event.played_with(player_one, player_two):
                return True

        return False
