"""Tools for interfacing to git bash

Initially to allow cygpath -w to be run
"""

import subprocess
from pathlib import Path


def win_path(path) -> Path:
    """Convert a potentially cygwin type path to windows format.
    """
    res = subprocess.run(["cygpath", "-w", str(path)], capture_output=True, text=True)
    if res.returncode:
        raise Exception(f"cygpath failed {res.stderr}")
    return Path(res.stdout.strip("\r\n"))
