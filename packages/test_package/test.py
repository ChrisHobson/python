"""A simple package that is used to validate that the test system locates us.

This package is used by the file tests/test_system/test_pytest.py
"""


def hello_func():
    """Simple function to check we can find the package

    The test loads this package and runs hello_func() just to be sure
    everything is working.
    """
    return "hello"
