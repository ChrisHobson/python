"""Runs compare tools, typically Winmerge.
"""

from pathlib import Path
import subprocess

from gitbash import win_path

def diff_two(ref_name: str, working_name: str):
    """Compares two files.

    ref_name is the original/reference file and is placed in to read only
    mode.

    name is the newer version of the file and can be edited.
    """

    merge_path = Path("/usr/bin/kdiff3")
    if not merge_path.is_file():
        # winmerge doesn't like certain cygwin style paths so convert to windows
        ref_path = win_path(ref_name)
        working_path = win_path(working_name)
        merge_path = Path("C:/Program Files/WinMerge/WinMergeU.exe")
        win = True
    else:
        ref_path = Path(ref_name)
        working_path = Path(working_name)
        win = False

    for file in [ref_path, working_path, merge_path]:
        if not file.is_file():
            raise Exception(f"{file} does not exist")

    if win:
        # /e allows a single press of ESC to close winmerge
        # /wl makes the lefthand file read only (The ref copy)
        subprocess.run([merge_path, "/e", "/wl", ref_path, working_path])
    else:
        # Linux uses kdiff3 and no arguments. This could be coded more nicely
        # but for two cases an if works fine.
        subprocess.run([merge_path, ref_path, working_path])

