"""Controllers, similar to the MVC paradigm.

Provides (potentially) different controllers for views.
"""
from maths.azel import AZEL
from maths.vector import Vector3D


class SphereController():
    """A controller than allows you to move a viewing point across a sphere.
    As the point moves so the view changes such that you always view the model
    towards the centre of the sphere. The up vector can also be changed, so you
    can spin on the spot. Equivalent to rotating the camera around its lens.
    """
    def __init__(self):
        """Create the default controller.
        """
        self._azel = AZEL()
        self._up = Vector3D(axis=Vector3D.VEC_J)
