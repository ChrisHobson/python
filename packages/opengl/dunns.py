"""Draw the Dunns combustion chamber using OpenGL.

Almost no code is present. This class exists just to
allow 'opengl dunns' to work and then forwards to the
opengl code in the dunns package.
"""
import dunns.dunns_opengl


class Dunns():
    def run(self, x_args):
        """Called via opengl program. Forward to dunns package.
        """
        dunns.dunns_opengl.DunnsOpenGL().dunns_opengl(x_args)
