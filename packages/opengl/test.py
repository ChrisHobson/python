"""Very simple OpenGL test which was downloaded from the internet.

Just confirms OpenGL is alive. Creates a Window and draws a shaded square.
This just proves OpenGL and OpenGL_Accelerate are installed. See
progs/opengl for details of installation.
"""
from OpenGL import *
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *


class Test:
    @staticmethod
    def run(x_args):
        w, h = 500, 500

        def square():
            glBegin(GL_QUADS)
            glVertex2f(100, 100)
            glVertex2f(200, 100)
            glVertex2f(200, 200)
            glVertex2f(100, 200)
            glEnd()

        def iterate():
            glViewport(0, 0, 500, 500)
            glMatrixMode(GL_PROJECTION)
            glLoadIdentity()
            glOrtho(0.0, 500, 0.0, 500, 0.0, 1.0)
            glMatrixMode(GL_MODELVIEW)
            glLoadIdentity()

        def show_screen():
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glLoadIdentity()
            iterate()
            glColor3f(1.0, 0.0, 3.0)
            square()
            glutSwapBuffers()

        glutInit()
        glutInitDisplayMode(GLUT_RGBA)
        glutInitWindowSize(500, 500)
        glutInitWindowPosition(0, 0)
        wind = glutCreateWindow("OpenGL Test")
        glutDisplayFunc(show_screen)
        glutIdleFunc(show_screen)
        glutMainLoop()
