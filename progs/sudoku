#!/usr/bin/env win_python
"""Sudoku solver.

Based on https://www.youtube.com/watch?v=G_UYXzGuqvM
"""
print("Sudoku")

# The board is made up of rows from 0-8 and columns from 0-8
main_board = [
    [5, 3, 0, 0, 7, 0, 0, 0, 0],  # Row 0
    [6, 0, 0, 1, 9, 5, 0, 0, 0],
    [0, 9, 8, 0, 0, 0, 0, 6, 0],

    [8, 0, 0, 0, 6, 0, 0, 0, 3],
    [4, 0, 0, 8, 0, 3, 0, 0, 1],
    [7, 0, 0, 0, 2, 0, 0, 0, 6],

    [0, 6, 0, 0, 0, 0, 2, 8, 0],
    [0, 0, 0, 4, 1, 9, 0, 0, 5],
    [0, 0, 0, 0, 8, 0, 0, 7, 9]   # Row 8
]


def print_board(board):
    for row in board:
        for num in row:
            print(f"{num} ", end="")
        print()


def get_left_cell(row, col):
    """Given a row column get the left centre row/col of the sub-square.
    """
    return (row // 3) * 3 + 1, (col // 3) * 3


def is_allowed(num, row, col, board):
    """Is the number allowed in the row/col?
    """
    # Is the number already in the row
    if num in board[row]:
        return False

    # Is the number already in the column
    for board_row in board:
        if board_row[col] == num:
            return False

    # Is the number already in the sub-square
    left_row, left_col = get_left_cell(row, col)
    # Check top row of sub-square
    if num in board[left_row - 1][left_col: left_col + 3]:
        return False
    # The sub-square centre row
    if num in board[left_row][left_col: left_col + 3]:
        return False
    # The row at the bottom of the sub-square
    if num in board[left_row + 1][left_col: left_col + 3]:
        return False

    return True


def any_zero(board):
    """Any blanks (or zeros) on the board?
    """
    for row in board:
        if 0 in row:
            return True

    return False


def solve(board):
    for row in range(9):
        for col in range(9):
            if board[row][col] == 0:
                for num in range(1, 10):
                    if is_allowed(num, row, col, board):
                        board[row][col] = num
                        solve(board)
                        board[row][col] = 0

                return

    print_board(board)


solve(main_board)
