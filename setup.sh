#
# Setups up to be able to run python project sripts
# by name by setting PATH and PYTHONPATH
#
# Must be "sourced", protects against multiple setups which
# would endlessly add to PATH
#
# You can source this file from other bash scripts/start up files
# to add in the paths. So for example in my Java project
# I could source this setup.sh to allow git difftool etc. to
# work as the difftool programs are Python and are in this
# git repo.
#
# The variable PYTHON_PROJ_SETUP is set to 'yes' when Python
# has been setup. This allows you to set it to 'no' in ~/.bash_login
# and then you can show the Python availability in the command
# prompt as a reminder to source the file
#
echo "Setting up Python, ensure you sourced this with '. setup.sh'"
if (test "$PYTHON_PROJ_SETUP" != "yes") then
    # path to this file so it can be run from anywhere
    # and it sets the paths correctly and absolutely
    if (test $(uname) != "Linux") then
      cwd=$(dirname $(cygpath -a $BASH_SOURCE))
      export PYTHONPATH="$(cygpath -mp $cwd/packages)"
    else
      # set $cwd to the absolute path to the python setup.sh
      cwd="$(pwd)/$(dirname $BASH_SOURCE)"
      pushd $cwd > /dev/null
      cwd=$(pwd)
      popd > /dev/null
      export PYTHONPATH="$cwd/packages"
    fi
    export PATH="$cwd/progs:$cwd/tests:$cwd/bin:$PATH"
    export PYTHON_PROJ_SETUP=yes
    unset cwd
else
  echo "Python is already setup"
fi
