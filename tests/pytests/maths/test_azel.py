"""Maths azel class.
"""
import unittest

from maths.azel import AZEL


class TestAZEL(unittest.TestCase):
    """Matrix classes.
    """

    def test_pos(self):
        """Test the position of a point at the angles.
        """
        azel = AZEL()

        xyz = [-1, -1, -1]

        azel.get_pos(10, xyz)
        self.assertAlmostEqual(xyz[0], 10)
        self.assertAlmostEqual(xyz[1], 0)
        self.assertAlmostEqual(xyz[2], 0)

        azel.az = 90
        self.assertEqual(azel.az, 90)
        azel.get_pos(10, xyz)
        self.assertAlmostEqual(xyz[0], 0)
        self.assertAlmostEqual(xyz[1], 10)
        self.assertAlmostEqual(xyz[2], 0)

        azel = AZEL(0, 90)
        azel.get_pos(10, xyz)
        self.assertAlmostEqual(xyz[0], 0)
        self.assertAlmostEqual(xyz[1], 0)
        self.assertAlmostEqual(xyz[2], 10)

        # Test setting el via property
        azel.el = 0
        self.assertEqual(azel.el, 0)
        azel.get_pos(10, xyz)
        self.assertAlmostEqual(xyz[0], 10)
        self.assertAlmostEqual(xyz[1], 0)
        self.assertAlmostEqual(xyz[2], 0)
