"""Maths Vector class.
"""
import unittest
from math import radians, sin, cos
from maths.vector import Vector3D


class TestVector(unittest.TestCase):
    """Matrix classes.
    """

    def test_init(self):
        """Test the construction and basic access methods.
        """
        vec = Vector3D()
        self.assertListEqual(vec._ijk, [0, 0, 0])
        vec = Vector3D(copy=[1, 2, 3])
        self.assertListEqual(vec._ijk, [1, 2, 3])
        vec = Vector3D(axis=Vector3D.VEC_I)
        self.assertListEqual(vec._ijk, [1, 0, 0])
        vec = Vector3D(axis=Vector3D.VEC_J)
        self.assertListEqual(vec._ijk, [0, 1, 0])
        vec = Vector3D(axis=Vector3D.VEC_K)
        self.assertListEqual(vec._ijk, [0, 0, 1])

        vec = Vector3D(copy=[1, 2, 3])
        self.assertEqual(vec[Vector3D.VEC_I], 1)
        self.assertEqual(vec.i, 1)
        self.assertEqual(vec[Vector3D.VEC_J], 2)
        self.assertEqual(vec.j, 2)
        self.assertEqual(vec[Vector3D.VEC_K], 3)
        self.assertEqual(vec.k, 3)

        vec2 = Vector3D(copy=vec)
        self.assertEqual(vec[Vector3D.VEC_I], 1)
        self.assertEqual(vec[Vector3D.VEC_J], 2)
        self.assertEqual(vec[Vector3D.VEC_K], 3)
        vec2.i = 9
        self.assertEqual(vec2.i, 9)
        vec2.j = 10
        self.assertEqual(vec2.j, 10)
        vec2.k = 11
        self.assertEqual(vec2.k, 11)

        with self.assertRaises(AssertionError):
            _ = vec[Vector3D.VEC_I - 1]
        with self.assertRaises(AssertionError):
            _ = vec[Vector3D.VEC_K + 1]
        with self.assertRaises(AssertionError):
            vec[Vector3D.VEC_I - 1] = 1
        with self.assertRaises(AssertionError):
            vec[Vector3D.VEC_K + 1] = 1

    def test_cross_dot(self):
        """Test the cross product.
        """
        vec_x = Vector3D(axis=Vector3D.VEC_I)
        vec_y = Vector3D(axis=Vector3D.VEC_J)
        # Will create and return a new Vector3D
        vec_z = vec_x.cross(vec_y)
        self.assertListEqual(vec_z._ijk, [0, 0, 1])
        # Orthogonal vectors will have a dot product of 0
        self.assertEqual(vec_z.dot(vec_y), 0)
        # Try static method
        self.assertEqual(Vector3D.dot_s(vec_z, vec_y), 0)

        # Will it work with arrays
        array_y = [0, 1, 0]
        array_z = ["a", "b", "c"]
        vec_x.cross(array_y, array_z)
        self.assertListEqual(array_z, [0, 0, 1])
        # Orthogonal vectors will have a dot product of 0
        # test static method works with arrays
        self.assertEqual(Vector3D.dot_s(array_y, array_z), 0)
        # Or

        vec_z2 = vec_z
        # Will update inplace an existing Vector3D and return it
        vec_z = vec_x.cross(vec_y, vec_z2)
        self.assertEqual(vec_z, vec_z2)
        self.assertListEqual(vec_z2._ijk, [0, 0, 1])

        # Try two normalised vectors at a few angles
        for degrees in range(0, 100, 10):
            rads = radians(degrees)
            sin_angle = sin(rads)
            cos_angle = cos(rads)
            a = Vector3D(copy=[cos_angle, sin_angle, 0])
            b = Vector3D(axis=Vector3D.VEC_I)
            dot = a.dot(b)
            self.assertEqual(cos_angle, dot)

    def test_length_normalise(self):
        """Test the length and normalise functions.
        """
        vec = Vector3D(copy=[3, 4, 0])
        self.assertEqual(vec.length(), 5)
        vec = Vector3D(copy=[0, 4, 3])
        self.assertEqual(vec.length(), 5)
        vec.normalise()
        self.assertEqual(vec.length(), 1)

        # Check default has length 0
        vec = Vector3D()
        self.assertEqual(vec.length(), 0)
        # and normalise doesn't / by 0
        vec.normalise()

    def test_project_to_plane(self):
        """Test project to a plane
        """
        vec = Vector3D(copy=[1, 1, 1])
        plane = Vector3D(axis=Vector3D.VEC_I)
        vec.project_to_plane(plane)
        self.assertListEqual(vec._ijk, [0, 1, 1])

        vec = Vector3D(copy=[1, 1, 1])
        plane = Vector3D(axis=Vector3D.VEC_J)
        vec.project_to_plane(plane)
        self.assertListEqual(vec._ijk, [1, 0, 1])

        vec = Vector3D(copy=[1, 1, 1])
        plane = Vector3D(axis=Vector3D.VEC_K)
        vec.project_to_plane(plane)
        self.assertListEqual(vec._ijk, [1, 1, 0])
