"""Maths matrix classes.
"""
import unittest
from math import pi
from maths.matrix import Matrix4x4


class TestMatrix(unittest.TestCase):
    """Matrix classes.
    """

    def test_matrix4x4(self):
        """Test the Matrix4x4 class.
        """
        matrix = Matrix4x4()
        # print(matrix)

    def test_matrix4x4_translation(self):
        """Test a translation matrix
        """
        matrix = Matrix4x4.create_translation((1, 2, 3))
        xyz = [10, 20, 30]
        matrix.xform_point(xyz)
        self.assertAlmostEqual(xyz[0], 11)
        self.assertAlmostEqual(xyz[1], 22)
        self.assertAlmostEqual(xyz[2], 33)

    def test_matrix4x4_concat(self):
        """Test concat of two matrices.
        """
        a = Matrix4x4.create_translation((1, 2, 3))
        b = Matrix4x4.create_translation((10.9, 11.2, 12.6))
        c = a.concat(b)
        xyz = [10, 20, 30.4]
        c.xform_point(xyz)
        # Test the total transform is as expected
        #                              xyz    a   b
        self.assertAlmostEqual(xyz[0], 10.0 + 1 + 10.9)
        self.assertAlmostEqual(xyz[1], 20.0 + 2 + 11.2)
        self.assertAlmostEqual(xyz[2], 30.4 + 3 + 12.6)

    def test_matrix4x4_rotate_x(self):
        """Test an X rotation.
        """
        a = Matrix4x4.create_x_rotation(pi / 2)
        xyz = [10, 15, 3]
        a.xform_point(xyz)
        self.assertAlmostEqual(xyz[0], 10.0)
        self.assertAlmostEqual(xyz[1], -3)
        self.assertAlmostEqual(xyz[2], 15)

    def test_matrix4x4_rotate_y(self):
        """Test a Y rotation.
        """
        a = Matrix4x4.create_y_rotation(pi / 2)
        xyz = [10, 15, 3]
        a.xform_point(xyz)
        self.assertAlmostEqual(xyz[0], 3)
        self.assertAlmostEqual(xyz[1], 15)
        self.assertAlmostEqual(xyz[2], -10)

    def test_matrix4x4_rotate_z(self):
        """Test a Z rotation.
        """
        a = Matrix4x4.create_z_rotation(pi / 2)
        xyz = [10, 15, 3]
        a.xform_point(xyz)
        self.assertAlmostEqual(xyz[0], -15)
        self.assertAlmostEqual(xyz[1], 10)
        self.assertAlmostEqual(xyz[2], 3)

    def test_matrix4x4_scale(self):
        """Test a scale matrix.
        """
        a = Matrix4x4.create_scale(2)
        xyz = [10, 15, 3]
        a.xform_point(xyz)
        self.assertAlmostEqual(xyz[0], 20)
        self.assertAlmostEqual(xyz[1], 30)
        self.assertAlmostEqual(xyz[2], 6)

