"""Very basic tests that check the system is working as expected.

Serve as an example of how to write tests
"""
import unittest
import json
import os
from pathlib import Path

from pytest import PyTest
# This checks that the environment is set up to locate modules within
# the packages directory
from test_package.test import hello_func


class TestBasic(unittest.TestCase):
    """Basic test class, mainly checking the system works.

    Provides some basic tests used to develop the framework and also
    checks the system is correctly configure.
    """
    def test_data(self):
        """Loads a data file to make sure test data can be found.
        """
        data_dir = PyTest.get().data_dir()
        self.assertTrue(data_dir.is_dir(), f"Missing {dir}")
        json_file_name = PyTest.get().data_file("one.json")
        self.assertTrue(json_file_name.is_file(), f"Missing {json_file_name}")
        with open(json_file_name, "r") as json_file:
            json_data = json.load(json_file)
        self.assertEqual(json_data['name'], "Adrian Mole")
        self.assertEqual(json_data['age'], 13.75)

    def test_inits(self):
        """Checks each pytests package directory has an __init__.py

        If a package doesn't have __init__.py then the unittest discover won't
        find test modules within the package and so tests won't run when they
        should be.

        The risk being that someone adds a new package and no tests get run.
        """
        tests_dir = PyTest.get().tests_dir()
        self.assertTrue(tests_dir.is_dir())
        num_dir = 0
        for dirpath, dirnames, filenames in os.walk(tests_dir):
            try:
                # Don't traverse the compiled python directories
                dirnames.remove("__pycache__")
            except:
                # remove will raise if the entry is missing, we don't care
                pass
            self.assertTrue(
                "__init__.py" in filenames,
                f"{dirpath} missing __init__.py"
            )
            num_dir += 1

        # Make sure we traversed some sub-directories
        self.assertGreater(num_dir, 4, "os.walk failed")

class TestPackages(unittest.TestCase):
    """Test that general packages can be found.

    Also useful for showing how to run a single test case of a class
    with multiple tests.
    """
    def test_hello(self):
        """Checks we can find proper packages.

        This is testing that the driver script has correctly setup
        sys.path to find packages that are part of "python" incase
        we want to test them. In truth the import at the top of this
        script will have failed if that is wrong, but check we can call
        the imported function.
        """
        self.assertEqual(hello_func(), "hello", "Function wrong")

    def test_dummy(self):
        """An empty test which exists to show one test can be run.

        The help for PyTest.run_one says a single test can be run by using

        pytests.test_system.test_pytests.TestPackage.test_hello

        so this dummy adds second test case just to show that

        run_tests --one pytests.test_system.test_pytests.TestPackages

        runs two tests, and that one test can be run as

        run_tests --one pytests.test_system.test_pytests.TestPackages.test_hello
        """
        pass
