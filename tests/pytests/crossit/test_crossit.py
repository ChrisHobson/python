"""Test for Crossit game.

One test class per game class, in one file. Keeps things together and simple.
"""
import unittest

from crossit.crossgame import CrossGame, CrossControl, CrossMessage

class CrossControlTest(CrossControl):
    def __init__(self, game, xy):
        """Setup the gaming engine and this controls x,y.
        """
        super().__init__(game, xy)
        self.is_on = False

    def state(self) -> bool:
        """Returns the state of the control, True if on()
        """
        return self.is_on

    def off(self):
        """Turn off the control.
        """
        self.is_on = False

    def on(self):
        """Turn on the control.
        """
        self.is_on = True


class CrossMessageTest(CrossMessage):
    def message(self, msg):
        """Display the message string, do nothing as automatic test.
        """
        pass
        # print(msg)

    def win(self):
        """Game has been won, application does whatever it wants.

        PLay a tune, do a dance.... it's your game your way!
        """
        print("WIN WIN WIN")


class TestCrossGame(unittest.TestCase):
    """Tests the class
    """
    def test_one(self):
        """A simple test.
        """
        game = CrossGame()
        message = CrossMessageTest()

        controls = list()
        for y in range(0,CrossGame.NUM_Y):
            x_controls = list()
            controls.append(x_controls)
            for x in range(0, CrossGame.NUM_X):
                x_controls.append(CrossControlTest(game, (x, y)))

        game.set_controls(message, controls)
        game.set_game(1)
        #self.print_board(controls)
        controls[2][2].hit()
        #self.print_board(controls)


    def print_board(self, controls):
        for row in controls:
            for control in row:
                if control.state():
                    print("X", end="")
                else:
                    print("0", end="")
            print("")

    def test_control_id_to_xy(self):
        """Tests the function works for various grid sizes.
        """
        xy = CrossGame.control_id_to_xy(1, 7, 6)
        self.assertTupleEqual(xy, (0, 0))

        for case in ((1, 0, 0), (2, 1, 0), (3, 2, 0),
                     (4, 0, 1), (5, 1, 1), (6, 2, 1)):
            xy = CrossGame.control_id_to_xy(case[0], 3, 2)
            self.assertTupleEqual(xy, case[1:])

        asserts = 0
        try:
            CrossGame.control_id_to_xy(0, 2, 2)
        except AssertionError:
            asserts += 1

        try:
            CrossGame.control_id_to_xy(5, 2, 2)
        except AssertionError:
            asserts += 1

        self.assertEqual(asserts, 2)
