"""Test for golftools.utils

"""
import unittest

from golftools.utils.members import Member, Members

class TestGolfToolsUtils(unittest.TestCase):
    """Tests the classes
    """
    def test_member(self):
        """Check the Member class.
        """
        Fred1 = Member("Fred")
        Fred2 = Member("Fred")
        Chris = Member("Chris")

        self.assertTrue(Fred1 == Fred2, "Two Fred's are equal")
        self.assertFalse(Fred1 == Chris, "Fred should not be a Chris")
        self.assertTrue(hash(Fred1) == hash(Fred2), "Two different Fred's hashes are equal")

        # Check multiple/leading/trailing spaces in names are removed
        Spaces = Member("  Chris    Hobson  ")
        self.assertEqual(Spaces.name, "Chris Hobson")

    def test_members(self):
        """Check the Members class.
        """
        Fred1 = Member("Fred")
        Fred2 = Member("Fred")
        Chris = Member("Chris")

        members = Members()
        members.add_member(Fred1)
        members.add_member(Chris)
        members_list = members.get_as_list()
        members_dict = members.get_as_dict()

        self.assertTrue(Fred1 in members_list, "Fred1 should be in the list")
        self.assertTrue(Chris in members_list, "Chris should be in the list")
        # Although not added as an object Fred2 should be in there
        # as we match on names
        self.assertTrue(Fred2 in members_list, "Fred2 should be in the list")

        self.assertTrue(Fred1.name in members_dict, "Fred1 should be in the dict")
        self.assertTrue(Chris.name in members_dict, "Chris should be in the dict")
        # Although not added as an object Fred2 should be in there
        # as we match on names
        self.assertTrue(Fred2.name in members_dict, "Fred2 should be in the dict")

        # If we add Fred1 and Fred2 to a dict we'd expect a single
        # actual addition as Member objects hash by name as being the same
        freds = dict()
        freds[Fred1] = 1
        freds[Fred2] = 2
        freds[Chris] = 3
        self.assertTrue(freds[Fred1] == 2, "Should only be 1 Fred in the dict")
        self.assertTrue(freds[Fred2] == 2, "Should only be 1 Fred in the dict")
        self.assertTrue(freds[Chris] == 3, "Chris should be in the dict")
