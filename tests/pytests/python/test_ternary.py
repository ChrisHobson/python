"""Ternary expressions.

"""
import unittest


class TestTernary(unittest.TestCase):
    @staticmethod
    def tern1(a, b):
        """A function which returns a if a < 10 else b.

        Uses the python style for a ternary.
        """
        return a if a < 10 else b

    def func0(self):
        """Returns 0 and counts calls.
        """
        self._num_func0 += 1

        return 0

    def func2(self):
        """Returns 2 and counts calls.
        """
        self._num_func2 += 1

        return 2

    def func10(self):
        """Returns 2 and counts calls.
        """
        self._num_func10 += 1

        return 10

    def reset(self):
        """Reset the call counts, must be called before any func*()
        """
        self._num_func0 = self._num_func2 = self._num_func10 = 0

    def test_ternary(self):
        self.assertEqual(self.tern1(8, 20), 8)
        self.assertEqual(self.tern1(10, 20), 20)

        # Check the or operator calls both functions when func1() returns 0
        self.reset()
        val = self.func0() or self.func10()
        self.assertEqual(val, 10)
        self.assertEqual(self._num_func0, 1)
        self.assertEqual(self._num_func10, 1)

        # Calling func2 shortcuts so func0 isn't called
        self.reset()
        val = self.func2() or self.func0()
        self.assertEqual(val, 2)
        self.assertEqual(self._num_func0, 0)
        self.assertEqual(self._num_func2, 1)

        # Testing and shortcuts when func1 returns 0
        self.reset()
        val = self.func0() and self.func2()
        self.assertEqual(val, 0)
        self.assertEqual(self._num_func0, 1)
        self.assertEqual(self._num_func2, 0)

        # Test and doesn't shortcut when func() is called first
        self.reset()
        val = self.func2() and self.func0()
        self.assertEqual(val, 0)
        self.assertEqual(self._num_func0, 1)
        self.assertEqual(self._num_func2, 1)

        # What does and return when both are true?
        # It returns the second value! actually it's the
        # last evaluated expression
        self.reset()
        val = self.func2() and self.func10()
        self.assertEqual(val, 10)
        self.assertEqual(self._num_func2, 1)
        self.assertEqual(self._num_func10, 1)

        self.reset()
        val = self.func10() and self.func2()
        self.assertEqual(val, 2)
        self.assertEqual(self._num_func2, 1)
        self.assertEqual(self._num_func10, 1)

        self.reset()
        val = self.func10() and self.func2() and self.func0()
        self.assertEqual(val, 0)
        self.assertEqual(self._num_func0, 1)
        self.assertEqual(self._num_func2, 1)
        self.assertEqual(self._num_func10, 1)
