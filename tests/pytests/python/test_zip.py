"""zip() investigation.

"""
import unittest


class Testzip(unittest.TestCase):
    """Basic zip test
    """
    def test_zip1(self):
        """Test zip() returns what we expect.
        """
        d1 = (1, 2, 3)
        d2 = (5, 6, 7, 8)

        expect = [(1, 5), (2, 6), (3, 7)]
        # zip stops when the 1st iterable ends, it returns an object
        zip_obj = zip(d1, d2)
        self.assertIsInstance(zip_obj, zip)
        res = list(zip(d1, d2))
        self.assertListEqual(res, expect)

        # Slightly different, range is used to generate 1,2,3,4
        # zip will end when d2 runs out of data
        res = list(zip(range(1, 10), d2))
        expect.append((4,8))
        self.assertListEqual(res, expect)
