"""enumerate() investigation.

"""
import unittest


class Testenumerate(unittest.TestCase):
    """Basic enumerate test
    """
    def test_enumerate(self):
        """Test enumerate() returns what we expect.

        enumerate returns a tuple where the first element is an item count
        which defaults to starting with 0, or a start argument can specify the
        initial count. The second element is the item from the iterable.
        """
        d1 = ("a", "b", "c")

        expect = [(1, "a"), (2, "b"), (3, "c")]
        self.assertListEqual(list(enumerate(d1, start=1)), expect)
