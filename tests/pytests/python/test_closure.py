"""Python closures.

"""
import unittest


class TestClosures(unittest.TestCase):
    """How closures work.
    """

    def test_closure(self):
        """Test closure creation and passing arguments.
        """

        f1, f2 = self.create_closures()
        # The variable "fiddle" is still referenced by the closure and
        # it had a value of "5" when create_closure returned it
        self.assertEqual(f1(2), 25)
        # Show that 2 closures access the same fiddle value
        f2(1)
        self.assertEqual(f1(2), 21)

    def create_closures(self):
        def mk_closure(x):
            def closure(y):
                # Fiddle is in the containing scope, its value is not captured
                # by the closure and so if its value changes after the closure
                # is made but before its called the returned value changes
                # as fiddle is used at call time
                return x * y + fiddle

            return closure

        def mk_fiddle_closure():
            """Create a second closure that allows fiddle's value to change.
            """
            def closure(new_fiddle):
                nonlocal fiddle
                fiddle = new_fiddle

            return closure

        fiddle = 3
        x = 10
        f1 = mk_closure(x)
        self.assertEqual(f1(6), 63)
        # x was captured from the argument when the closure was created, and
        # so changing this local x doesn't change the result
        x = 50
        self.assertEqual(f1(6), 63)
        # Changing fiddle changes the result
        fiddle = 5
        self.assertEqual(f1(6), 65)

        # Make a closure that can change fiddle
        f2 = mk_fiddle_closure()
        # Prove it changes the one true fiddle
        f2(7)
        self.assertEqual(fiddle, 7)
        # Put back to 5
        fiddle = 5
        # Return the closures so they can be called again after fiddle has gone
        # out of scope, to show it is still referenced by both closures
        return f1, f2
