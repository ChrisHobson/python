"""Python Classes.

"""
import unittest


class TestClasses(unittest.TestCase):
    """How various class features work.
    """

    def test_getitem(self):
        """Test how you can implement [] in a class.
        """
        class NotScriptable:
            """A class that cannot be accessed via []
            """
            pass
        a = NotScriptable()
        with self.assertRaises(TypeError):
            b = a[1]

        class Scriptable:
            """A class that can.
            """
            def __init__(self):
                self._list = ['a', 'b', 'c', 'd']

            def __getitem__(self, item):
                return self._list[item]

            def __setitem__(self, key, value):
                self._list[key] = value

        a = Scriptable()
        self.assertEqual(a[0], 'a')
        self.assertEqual(a[3], 'd')

        a[2] = 5
        self.assertEqual(a[2], 5)

        # This basic implementation can be sliced as
        # under the hood it's a list. However, a different
        # class would need to add slicing
        self.assertListEqual(a[0:2], ['a', 'b'])

        # And we can set via a slice
        a[0:2] = 11,22
        self.assertListEqual(a[0:2], [11, 22])
