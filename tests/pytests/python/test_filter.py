"""filter() investigation.

"""
import unittest


class Testfilter(unittest.TestCase):
    """Basic filter test
    """
    def test_filter(self):
        """Test filter() returns what we expect.

        """
        d1 = range(1,6)

        expect = [1, 2]
        # filter returns items for which the function returns true
        f = filter(lambda x: x < 3, d1)
        # Actually it returns a filter object
        self.assertIsInstance(f, filter)
        self.assertListEqual(list(filter(lambda x: x < 3, d1)), expect)
