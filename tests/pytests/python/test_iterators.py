"""Test how to write iterators.
"""
import unittest


class TestIterators(unittest.TestCase):

    def test_simple(self):
        """Test the simple case of a class that can be iterated.

        The issue with this approach is that the object stores its
        own iteration state and so cannot be used twice.
        """
        class Iterable:
            """How to add a simple iterator to a class.
            """

            def __init__(self, num):
                self.m_num = num
                self.m_curr = 0

            def __iter__(self):
                self.m_curr = 0
                return self

            def __next__(self):
                self.m_curr = self.m_curr + 1
                if self.m_curr > self.m_num:
                    raise StopIteration()

                return self.m_curr

        iterable = Iterable(10)

        # Check we get a sequence of 1-10
        # We could do this
        # are_equal = all(a == b for a, b in zip(i, range(1, 11), strict=True))
        # but it's a bit convoluted when we can say

        self.assertListEqual(list(iterable), list(range(1, 11)))

        # When we use the iterable twice in the same zip we
        # find that as they share the same counter we get
        # pairs returned
        self.assertListEqual(
            [(1, 2), (3, 4), (5, 6), (7, 8), (9, 10)]
            , list(zip(iterable, iterable, strict=True))
        )

    def test_class(self):
        """Test using an interator that returns a class to do the iteration

        This approach uses an external object to store iteration state
        and so multiple independent iterators can operate on the same object.
        """
        class Iterable:
            def __init__(self, num):
                self.m_num = num

            def __iter__(self):
                return Iterable.InternalIter(self)

            class InternalIter:
                """A class that allows iterable to be iterated multiple times
                """
                def __init__(self, external):
                    self.m_ext = external
                    self.m_curr = 0

                def __next__(self):
                    self.m_curr = self.m_curr + 1
                    if self.m_curr > self.m_ext.m_num:
                        raise StopIteration()

                    return self.m_curr

        # Make an instance and then iterated it twice
        # As we get a separate object returned for each
        # iteration we get two repeats of the same values
        iterable = Iterable(4)

        one = [x for x in iterable]
        two = [x for x in iterable]

        # Test the values
        self.assertListEqual([1, 2, 3, 4], one)
        # and that both lists are the same
        self.assertListEqual(one, two)
