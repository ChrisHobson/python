"""Python lambda.

"""
import unittest


class TestLambda(unittest.TestCase):
    """How lambda functions work
    """
    @staticmethod
    def hide_lambda_warning(lamb):
        """Pycharm complains (rightly, probably) if assigning a lambda.

        Hide the warning by passing the lambda to a function that returns it.

        f = lambda: value

        will warning, where as this won't

        f = self.hide_lambda_warning(lambda: value)
        """
        return lamb

    def test_lambda(self):
        """Test creation and passing arguments

        Shows how the lambda accesses variables in its outer scope which
        can change between the creation and call of the lambda. This can be
        avoided by passing arguments to the lambda as default arguments, to
        capture the values at lambda creation time.
        """

        value = 10
        f = self.hide_lambda_warning(lambda: value)
        lambda_val = f()
        self.assertEqual(f(), 10)
        # The lambda uses the value from the outer scope so changes to
        # variable change the return from the lambda
        value = 11
        self.assertEqual(f(), 11)

        # The value can be captured by passing it as a default argument
        f = self.hide_lambda_warning(lambda val=value: val)
        self.assertEqual(f(), 11)
        value = 12
        self.assertEqual(f(), 11)
