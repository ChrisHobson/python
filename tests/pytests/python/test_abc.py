"""Abstract Base Class investigation.

This uses my unit test system as a nice way of writing code to investigate
Python language features.
"""
import unittest
from abc import ABC, abstractmethod


class Abstract(ABC):
    """And abstract class with an __init__ method and abstract value()"""
    def __init__(self, v):
        self.v = v

    @abstractmethod
    def value(self):
        pass

class Concrete(Abstract):
    """A concrete class which implements value()

    The concrete class passing v to the abstract base class which stores
    it. This is just a means of checking abstract classes can provide some
    concrete methods.
    """
    def __init__(self, v):
        super().__init__(v)

    def value(self):
        """Implements a method to return v as passed to constructor.
        """
        return self.v

class ABC1(unittest.TestCase):
    """Basic ABC test
    """
    def test_abstract(self):
        """Tests we cannot instantiate an abstract class
        """
        try:
            x = Abstract()
            self.assertIsNone(x, "Shouldn't get here")
        except TypeError:
            # We expect a TypeError, so the test is good if we get one
            pass

    def test_concrete(self):
        """Test we can instantiate a Concrete and it calls abstract __init__"""
        x = Concrete(15)
        self.assertEqual(x.value(), 15, "value() wrong")
