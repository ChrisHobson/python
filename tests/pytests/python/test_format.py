"""Python f string samples.
"""

import unittest


class TestFormat(unittest.TestCase):
    def test_format(self):
        """Test for format strings to learn how they work.
        """
        a = 10
        b = 3.217652

        # Left and right aligned
        s = f"xx{a:<10}xx{a:>10}xx"
        self.assertEqual(s, "xx10        xx        10xx")

        # Various precisions
        s = f"{b:.2f} {b:3.3f} {b:.4f} {b:.5f} {b:.6f}"
        self.assertEqual(s, "3.22 3.218 3.2177 3.21765 3.217652")

        # Various filed widths
        s = f"xx{b:5.2f}xx{b:10.3f}xx{b:<8.2}"
        self.assertEqual(s, "xx 3.22xx     3.218xx3.2     ")

        # Array index
        arr = [1, 2, 3]
        s = f"x{arr[2]}x"
        self.assertEqual(s, "x3x")
