"""Python sorting.

The Python documentation is a bit vague on some of the sorting issues. This
test shows various ways of sorting a set of data and gets different results
depending on how we sort or more correctly what we consider the data to be.
"""
from functools import cmp_to_key
import unittest


class TestSorting(unittest.TestCase):
    """How sorting works.
    """

    def test_sort(self):
        """Test sorting methods and techniques.

        Sort the same "data" various ways and check we get the expected result,
        also use different techniques to sort to show some of the options.

        The data is a list of strings, which we can consider to be
        1. Strings (obviously), here 14.2 < 2.2
        2. Strings representing floats, here 14.2 is the highest value and
           3.10 < 3.9
        3. Strings representing software version numbers, where (for example)
           3.10 > 3.9 (as 10 is > 9).
        """
        # The data to sort
        data = ["3.7", "2.2", "2.10", "5.6", "3.10", "3.9", "14.2"]
        # The expected results, notice how all 3 results has different orders
        # depending on how the data is interpreted
        #
        # Expected ascii order
        # Notice that 14.2 is the lowest ascii value due to the 1, but
        # is the highest version or numeric value
        expected_ascii = ["14.2", "2.10", "2.2", "3.10", "3.7", "3.9", "5.6"]
        #
        # Expected version number order, versions are compared major first and
        # minor second, where 3.10 is > 3.9 which is not the case for numeric
        # compares
        expected_version = ["2.2", "2.10", "3.7", "3.9", "3.10", "5.6", "14.2"]
        #
        # Expected numeric order. In this compare 3.10 < 3.9 as actually its 3.1
        expected_numeric = ["2.10", "2.2", "3.10", "3.7", "3.9", "5.6", "14.2"]

        # ASCII sort, the simple default
        # sorted() returns a new list and does not change the input
        data_sorted = sorted(data)
        self.assertListEqual(data_sorted, expected_ascii)

        def compare(a, b):
            """A old style function to compare the int and decimal portions
            See
            https://docs.python.org/3/howto/sorting.html#the-old-way-using-the-cmp-parameter

            This is clever, a class gets made (by cmp_to_key) which overloads
            all of the > < etc. operators and forwards them to the compare
            function.

            The constructor of the class is called when sorted function
            invokes the 'key' function, and that constructor is passed the
            "value" as an argument. Then internals of sorted then,
            one assumes, calls the operators via the returned value (the
            class object) of the 'key' function.

            The documentation/implementation of cmp-to_key isn't clear as to
            the class overloads all of the comparison operators, as far as I
            can see only the < (__lt__) operator is used by sorting.

            This method returns
            -ve a < b (when compared as software versions)
            +ve a > b
            0   a == b
            """
            # This is probably the way I'd have written this pre-python and
            # pre-more-knowledge of sorting. In perl for example I certainly
            # would have done this work long hand as shown...
            a_int, a_dec = a.split(".")
            b_int, b_dec = b.split(".")

            if a_int == b_int:
                # Compare the major portion of version
                return int(a_dec) - int(b_dec)
            else:
                # Compare the minor portion of the version
                return int(a_int) - int(b_int)

        data_sorted = sorted(data, key=cmp_to_key(compare))
        self.assertListEqual(data_sorted, expected_version)

        class Sorter:
            """A more pythonic sorter.

            A class, which is created once per value, which compares versions.
            __lt__ is overloaded and the is called by the sort method, although
            this doesn't appear to be documented very clearly. I deduced this
            by the way cmp_to_key() works.

            This example sorts muhc like the function variant comparing values
            by hand. It could however of split the string in to an integer
            tuple(major, minor), but we do that later as a lambda example.

            This is just an example after all, and not a recommended way of
            coding.
            """
            def __init__(self, x):
                # Split to integers
                self.major, self.minor = [int(v) for v in x.split(".")]

            def __lt__(self, other):
                # Compare the pre-split key values
                if self.major < other.major:
                    return True
                elif self.major == other.major:
                    return self.minor < other.minor

                return False

        data_sorted = sorted(data, key=Sorter)
        self.assertListEqual(data_sorted, expected_version)

        # Similar to the Sorter variant except use a tuple, python tuples
        # compare the way we want for software versions, checking each element
        # in turn. Note the use of map() to convert each portion of the
        # split() to integer. map() returns a generator which is looped over
        # by tuple() to create a tuple (major, minor)
        data_sorted = sorted(data, key=lambda s: tuple(map(int, s.split("."))))
        self.assertListEqual(data_sorted, expected_version)

        # And finally another lambda which compares as simple numerics
        data_sorted = sorted(data, key=lambda s: float(s))
        self.assertListEqual(data_sorted, expected_numeric)
