"""Python list comprehension examples that I have crafted.
"""
import unittest


class TestListComprehension(unittest.TestCase):
    """List comprehensions.
    """

    def test_one(self):
        """Test various complexities of list comprehension.
        """

        one = [x for x in "abcd"]
        self.assertListEqual(one, ["a", "b", "c", "d"])

        two = [x for x in "abcd" if x in "ab"]
        self.assertListEqual(two, ["a", "b"])

        three = [x for x in range(1, 100) if x % 20 == 0]
        self.assertListEqual(three, [20, 40, 60, 80])

        # 3 nested loops showing the way a nested comprehension works
        # that is the inner loops changes the fasted which matches the
        # right most for in the comprehension
        xyz = list()
        for x in ["x1", "x2"]:
            for y in ["y1", "y2"]:
                for z in ["z1", "z2", "z3"]:
                    xyz.append((x, y, z))

        # The same as above but as a list comprehension
        four = [(x, y, z)
                for x in ["x1", "x2"]
                for y in ["y1", "y2"]
                for z in ["z1", "z2", "z3"]
                ]

        self.assertListEqual(four, xyz)
