"""Handles the running of the tests
"""

import unittest
from pathlib import Path

class PyTest:
    _singleton = None

    def __init__(self, start_dir: Path, data_dir: Path):
        """Sets up the tests object.

        start_dir - where the test system is located, basically the directory
        containing the run_python_tests script and the parent directory below
        which the 'pytests' package is located.

        data_dir - the directory containing the shared test data
        """
        assert isinstance(start_dir, Path)
        assert isinstance(data_dir, Path)

        self._start_dir = start_dir
        self._data_dir = data_dir

        assert not PyTest._singleton, "Should only be one instance"
        PyTest._singleton = self

    @staticmethod
    def get():
        """Returns the one true instance."""
        assert PyTest._singleton, "No instance created"
        return PyTest._singleton

    def run_all(self, verbosity):
        """Discover all tests below the start_dir and run them.
        """
        tests = unittest.TestLoader().discover(start_dir=str(self._start_dir))
        unittest.TextTestRunner(verbosity=verbosity).run(tests)

    def run_one(self, test_name, verbosity):
        """Run one test.

        test_name can be a full test module name (the filename effectively)
        this runs all tests within all test classes in the module
        pytests.test_system.test_pytests

        a class within the module, this runs all tests within the class
        pytests.test_system.test_pytests.TestBasic

        a single test_xxx within the class
        pytests.test_system.test_pytests.TestPackage.test_hello
        """
        tests = unittest.TestLoader().loadTestsFromName(test_name)
        unittest.TextTestRunner(verbosity=verbosity).run(tests)

    def data_dir(self) -> Path:
        """Directory below which there are test data files.
        """
        return self._data_dir

    def data_file(self, name: Path) -> Path:
        """Return full path to the name test data file
        """
        return self._data_dir / name

    def start_dir(self):
        """The start directory.

        Where we start scanning for tests, one level above pytests
        """
        return self._start_dir

    def tests_dir(self) -> Path:
        """Where the tests directory (base package) is.

        Tests are all within the pytests package within tests. This returns
        the root directory, that is "<path>/tests/pytests"
        """
        return self._start_dir / "pytests"
